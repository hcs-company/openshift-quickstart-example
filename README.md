# OpenShift Quickstart Example

Dit repo bevat een voorbeeld van een OpenShift Quickstart.

## Installatie
1. `oc create -f hcs-demo-quickstart.yml`

## Gebruik
1. Navigeer in de OpenShift Web Console naar **Help**→**Quick Starts**
2. Selecteer **Installeer de Web Terminal Operator**
